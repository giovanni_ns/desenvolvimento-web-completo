# Projeto 3 - TecBlog

O projeto de blog que vamos desenvolver é uma introdução ao HTML e CSS básico, ideal para iniciantes que desejam aprender a construir páginas da web simples. O objetivo é criar uma estrutura básica de blog e estilizá-la usando CSS para dar uma aparência mais agradável e profissional.
