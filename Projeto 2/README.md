# Projeto 2 - Anna Bella

Este portfólio foi criado especialmente para Anna Bella, com o objetivo de destacar suas habilidades, experiências e projetos em um formato visualmente atraente e profissional. Ao desenvolvê-lo, foram utilizadas técnicas simples, porém eficazes, de CSS e semântica HTML para garantir uma estrutura bem organizada e uma apresentação agradável.

No processo de criação deste portfólio, foram empregados princípios de design que visam transmitir uma identidade única e cativante. A escolha cuidadosa das cores, fontes e layouts proporciona uma experiência visualmente harmoniosa aos visitantes, permitindo que eles se envolvam e apreciem o conteúdo apresentado.

Utilizando as melhores práticas de semântica HTML, cada elemento foi estruturado de forma lógica e significativa, tornando a navegação pelo portfólio intuitiva e facilitando a compreensão das informações fornecidas. A marcação adequada dos títulos, parágrafos, listas e imagens garante a acessibilidade e a indexação correta do conteúdo pelos mecanismos de busca.

No que diz respeito ao estilo, as técnicas de CSS aplicadas trouxeram vida ao design do portfólio. Foram utilizadas classes e seletores para aplicar estilos específicos aos elementos, garantindo uma aparência coesa e consistente em todo o site. Além disso, foram empregadas propriedades como margens, preenchimentos e bordas para criar espaçamento adequado e destacar elementos-chave.

A responsividade também foi considerada durante o desenvolvimento deste portfólio. Com o uso de media queries e técnicas de layout fluido, o site se adapta de maneira elegante e harmoniosa a diferentes dispositivos, como smartphones, tablets e desktops, proporcionando uma experiência de visualização otimizada em qualquer tamanho de tela.

Além de apresentar informações sobre sua formação, habilidades e experiências profissionais, este portfólio também destaca alguns dos projetos mais relevantes realizados por Anna Bella. Cada projeto é acompanhado de uma breve descrição, capturas de tela e links para acesso ou visualização do trabalho completo, permitindo que os visitantes explorem e se envolvam com os resultados obtidos.
