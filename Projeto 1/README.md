# Projeto 1 - UNES

Vamos considerar um exemplo de um site universitário de uma faculdade construído exclusivamente em HTML, utilizando tabelas para a estruturação do conteúdo. No entanto, é importante destacar que o uso de tabelas para criar layouts de sites não é mais recomendado na prática moderna de desenvolvimento web.

Antigamente, as tabelas eram amplamente utilizadas para organizar o conteúdo visualmente, mas essa abordagem possui várias limitações e problemas. Alguns motivos pelos quais as tabelas não são mais recomendadas para criar sites incluem:

1. Semântica inadequada: O objetivo principal das tabelas é exibir dados tabulares, como planilhas ou dados organizados em colunas e linhas. Utilizar tabelas para criar layouts de sites não segue a estrutura semântica adequada, dificultando a compreensão do conteúdo pelos mecanismos de busca e leitores de tela para acessibilidade.

2. Dificuldade de manutenção: À medida que o tamanho e a complexidade do site aumentam, a utilização de tabelas para estruturar o conteúdo se torna cada vez mais complicada e difícil de manter. Mudanças simples no layout podem exigir a alteração de várias tabelas, o que torna o código mais confuso e propenso a erros.

3. Responsividade comprometida: Com o advento dos dispositivos móveis, é essencial que os sites sejam responsivos, ou seja, capazes de se adaptar a diferentes tamanhos de tela. O uso de tabelas dificulta a criação de layouts responsivos, tornando a experiência do usuário inconsistente em dispositivos móveis e comprometendo a usabilidade.

4. Desempenho e carregamento lento: Utilizar tabelas para criar layouts resulta em uma quantidade excessiva de código HTML desnecessário. Isso pode levar a tempos de carregamento mais lentos, impactando negativamente a experiência do usuário e o SEO do site.

Atualmente, as melhores práticas de desenvolvimento web envolvem o uso de elementos semânticos HTML, como divs, para criar a estrutura do layout, e estilos CSS para controlar o posicionamento e o design visual. Essa abordagem oferece mais flexibilidade, facilidade de manutenção, melhor desempenho e capacidade de criar sites responsivos.

Portanto, é altamente recomendado evitar o uso de tabelas para criar layouts de sites e adotar as práticas modernas de desenvolvimento web, seguindo as diretrizes semânticas e aproveitando as capacidades do HTML e do CSS para criar sites mais eficientes e acessíveis.
